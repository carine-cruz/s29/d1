

//import express module in the application
const express = require('express');


const app = express() //createServer()
//express() is the servers stored in  the variable app

const PORT = 3005;

// Middlewares
//parses, incoming json body payload
//declared before routes
app.use(express.json())
//like json, this parses data coming from forms
app.use(express.urlencoded({extended:true}))

/*
HANDLING ROUTES WITH HTTP METHOD

syntax:

app.method(<url endpoint(string)>, <request listener funtion>)
*/

//app.get("/", (req, res) => console.log(`Hello from get method`));
//app.get("/", (req, res) => res.send(`Hello from get route`));
app.get("/", (req, res) => res.status(202).send(`Hello get from get route`));

app.post("/hello", (req, res) => {
	
	//console.log(req.body); //{ firstName: 'Chester', lastName: 'Clavio' }
	//console.log(typeof req.body) //returns object
	const {firstName, lastName} = req.body;

	res.send(`Hello, my name is ${firstName} ${lastName}`);

	//refactor this by using request body to show dynamic output on the response by showing dynamic names
		//A: deconstruct req.body
})


/* Mini Activity:
	create a route endpoint for "/signup" endpoint that will add a document to a mock database

	if username and password are not empty, add this new document in the mock database then send a response "user <name> successfully registered" the mock database back to as a response back to the client

	else send back a response "please input both username and password"
*/
/*app.post(`/signup`, (req, res) => {
	{username, password} = req.body;

	res.send(`logged`);

	if (username !== ``) && (password !== ``){
		userArr.push({
			"username": username,
			"password": password
		});
		res.send(`User ${username} successfully registered.`);
	} else {
		res.send(`Please input both username and password.`);
	}
})*/

//solution:
const user = [
	{ "userName": "carinecruz", 
	   "password": "123"
	},
	{
		"userName": "anghelito",
		"password": "123"
	}
];

app.post("/signup", (req, res) => {

	//res.send(`successful`);

	const {userName, password} = req.body;

	if (userName !== undefined && password !== undefined) {
		user.push(req.body);

		res.send(`User ${userName} has been registered.`);
	} else {
		res.status(400).send(`Please input both username and password.`);
	}
});

/* Mini Activity
	Create a route that will be able to update the password and and return a message User <name>'s password has been updated. Else, user does not exist

	Clue: use loop and assign the new pw from the old pw

	check using console.log if password has been updated
*/

//console.log(user)

//My answer: (no loop)
/*
app.patch("/change-password", (req, res) => {
	const { userName, password} = req.body;

	if (userName !== undefined && password !== undefined) {
		
		res.send(`User ${userName}'s password has been updated`);
		console.log(req.body);

	} else {
		res.send(`User does not exist`);
	}
})
*/

app.patch("/change-password", (req, res) => {
	//console.log(req);

	const {userName, password} = req.body;
	console.log(user)

	for(let i = 0; i < user.length; i++){
		if(user[i].userName === userName){
			user[i].password = password;
			message = `User ${userName}'s password has been updated.`;
			break;
		} else {
			message = `User does not exist.`;
		}
	}
	res.status(200).send(message);
	console.log(user);
})


/* LISTEN METHOD */

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))